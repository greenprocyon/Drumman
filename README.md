# Drumman

Drum synthesizer written in Rust!*

*this drum synth does not exist yet give it a few years

TODO:
- [ ] Actually have a proof of concept
- [ ] FM synthesis
- [ ] Wavetable capabilities

WANT:
- [ ] Granular capabilities
- [ ] Vast preset library
- [ ] Good looking GUI